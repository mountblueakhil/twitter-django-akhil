from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from twitterapp import views




urlpatterns = [
    path('',views.index,name='index'),
    path('login',views.login1,name='login'),
    path('logout',views.logout,name='logout'),
    path('register',views.register,name='register'),
    path('updateProfile',views.updateProfile,name='updateProfile'),
    path('deletetweet/<int:id>',views.deleteTweet,name='deletetweet'),
    path('createTweet',views.createTweet,name='createTweet'),
    path('tweet/<int:id>',views.gettweet,name='gettweet'),
    path('deleteAccount/<int:id>',views.deleteAccount,name='deleteAccount'),
    path('users',views.getusers,name='getusers'),
    path('user/<int:id>',views.getuser,name='getuser'),
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
