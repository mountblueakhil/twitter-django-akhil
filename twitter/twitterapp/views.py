from django.contrib import messages
from django.shortcuts import redirect, render
from django.contrib.auth.models import User,auth
from django.contrib.auth import login ,authenticate
from .models import Profilemodel,Post

class MyException(Exception):
    pass
# Create your views here.
def index(request):
    tweets = Post.objects.all()
    return render(request, 'index.html',{'tweets':tweets})

def login1(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password=password)
        # print(username)
        if user is not None:
            auth.login(request,user)

            return redirect('/')
        else :
            messages.info(request,"invalid credentials")
            return redirect('login')

    else :
        return render(request,'login.html')


def register(request):
    if request.method == 'POST':
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']


        if User.objects.filter(username = username).exists():
            messages.info(request, "username taken")
            return redirect('register')
        elif User.objects.filter(email = email).exists():
            messages.info(request, "email taken")
            return redirect('register')
        else :
            user = User.objects.create_user(username=username,
                                            password=password, email=email, first_name= firstname, last_name = lastname)
            # print(user)
            user.save()
            if user :
                user = authenticate(username=username,password=password)
                if user is not None:
                    login(request, user)
                    return redirect('updateProfile')
            # return render(request,'updateProfile.html')

    else :
        return render(request, 'register.html')
def updateProfile(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            if Profilemodel.objects.filter(user = request.user).exists():
                bio = request.POST['bio']
                profilepic = request.FILES['profilepic']
                nickname = request.POST['nickname']
                profiles = Profilemodel.objects.filter(user = request.user).update(image=profilepic,bio=bio,nickname=nickname)
            else :
                bio = request.POST['bio']
                profilepic = request.FILES['profilepic']
                nickname = request.POST['nickname']
                profiles = Profilemodel.objects.create(user=request.user,image=profilepic,bio=bio,nickname=nickname)
            return redirect('/')
        else:
            return render(request,'updateprofile.html')
    else :
        raise MyException('please login')
        # return redirect('login')

def logout(request):


    auth.logout(request)
    return redirect('login')


def deleteAccount(request,id):
    if request.method == 'GET':
        userid = id
        user = User.objects.get(id=userid)
        print(user)
        user.delete()


        return redirect('login')

def getusers(request) :
    if request.user.is_authenticated:
        users = User.objects.all()
        
        return render(request, 'users.html',{'users':users})    
    else :
        raise MyException('please login')

def getuser(request,id) :
    if request.user.is_authenticated:
        userid = id
        # profile = Profilemodel.objects.get(user=request.user)
        print("ok0")
        user1 = User.objects.get(id=userid)
        print(user1)
        print("ok0")
        profile = Profilemodel.objects.get(user=user1)
        posts = Post.objects.filter(profileuser=profile).order_by('-time')

        return render(request, 'profile.html',{'users':user1,'posts':posts})    
    else :
        raise MyException('please login')

def createTweet(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            profile = Profilemodel.objects.get(user=request.user)
            postdesc = request.POST['desc']
            file = request.FILES['image']
            # print(file)
            posts = Post.objects.create(postImg=file,profileuser=profile,user=request.user,text=postdesc)
            return render(request,'getuser')
        else:
            messages.info(request, "something went wrong")
            return render(request,'createppost.html')
    else:
        raise MyException('please login')
        # return redirect('login')

def gettweet(request,id):
    if request.method == "POST":
        # user = request.user
        # tweetid =request.POST.get('id')
        # print(tweetid)
        tweets= Post.objects.get(id=id)
        return render(request,'index.html',{'tweets':[tweets]})
        
        
def deleteTweet(request,id):
    tweetid = id
    tweet = Post.objects.get(id=tweetid)
    tweet.delete()
    print(tweet)

    return redirect('/')